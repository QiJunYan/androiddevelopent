// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()

const db=cloud.database()
const _=db.command
// 云函数入口函数
exports.main = async (event, context) => {
  let num=await db.collection('relatives').where({
    event_id:parseInt(event.event_id),
    joiner_id:event.joiner_id
  }).get()
  return db.collection("relatives").doc(num.data[0]._id)
  .update({
   data:{
     state:1
   }
 })
}