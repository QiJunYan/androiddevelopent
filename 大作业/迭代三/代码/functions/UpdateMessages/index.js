// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()

const db=cloud.database()
const _=db.command
// 云函数入口函数
exports.main = async (event, context) => {
  db.collection("messages").doc(event.doc)
  .update({
   data:{
     message:_.push({
       openid:event.openid,
       content:event.input_content,
       createTime:event.time,
       type:event.type
     })
   }
 })
}