// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db=cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
 return await db.collection('user').add({
   data:{
    openid:event.openid,
    avatarUrl:event.avatarUrl,
    nickName:event.nickName,
    serve_time:0,
    activity_num:0,
    point:0,
    id:"",
    birth:"",
    age:"",
    declaration:"",
    sex:"",
    lastlogin:""
   }
 })
}