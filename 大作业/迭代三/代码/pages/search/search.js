// pages/search/search.js
import Dialog from '../../miniprogram_npm/tdesign-miniprogram/dialog/index';
Page({
data: {
    value: '',
    allEvents:[],
    showList:[],
    history_tags:[],
    tags: [ '防控疫情',"交通治安" ,"普法宣传","大型活动","环境保护","帮困助弱","文明实践","文体科技"],
},
onLoad(){
  this.refresh()
},
onShow(){
  this.refresh()
},
tag(e){
  var temp=this.data.tags[e.currentTarget.dataset.index]
  this.set(temp)
  this.setData({
    value:temp
  })
  this.search(temp)
},
delete_history(){
  if(this.data.history_tags.length!=0){
    Dialog.confirm({
      title: '删除历史记录',
      content: '注：删除后不可恢复',
      confirmBtn: '确定',
      cancelBtn: '取消',
    })
      .then(() => {
        this.setData({
          history_tags:[]
        })
        wx.setStorage({
          key:'history_tags',
          data:[]
        })
      })
      .catch(() => {
        // 点击取消按钮
      });
  }
},
tag_history(e){
  var temp=this.data.history_tags[e.currentTarget.dataset.id]
  this.setData({
    value:temp
  })
  this.search(temp)
  if(this.data.history_tags[0]!=temp){
    let t=this.data.history_tags[0]
    this.data.history_tags[0]=temp
    this.data.history_tags[e.currentTarget.dataset.id]=t
    this.setData({
      history_tags:this.data.history_tags
    })
    wx.setStorage({
      key:'history_tags',
      data:this.data.history_tags
    })
  }
},
refresh(){
  var that=this
  wx.getStorage({
    key:'history_tags',
    success(res){
      that.setData({
        history_tags:res.data
      })
    }
  })
  wx.cloud.database().collection("events").get().then(res=>{
    that.setData({
      allEvents:res.data
    })
  })
  .catch(err=>{
    console.log("失败",err)
  })
},
set(data){
  if(this.data.history_tags.indexOf(data)<0){
    var temp=[]
      temp.push(data)
      temp=temp.concat(this.data.history_tags)
      if(temp.length>9){
        temp= temp.slice(0,9)
        }
      this.setData({
        history_tags:temp
      })
      wx.setStorage({
        key:'history_tags',
        data:temp
      })
  }
  else{
    let temp=this.data.history_tags.indexOf(data)
    let t=this.data.history_tags[0]
    this.data.history_tags[0]=this.data.history_tags[temp]
    this.data.history_tags[temp]=t
    this.setData({
      history_tags:this.data.history_tags
    })
    wx.setStorage({
      key:'history_tags',
      data:this.data.history_tags
    })
  }      
},

onActionClick(){
  this.set(this.data.value)
  this.search(this.data.value)
},
submit(e){
  this.set(e.detail.value)
  this.search(e.detail.value)
},
change(e){
  this.search(e.detail.value)
},
search(data,advancedata=[]){
  if(data!=""){
    var that=this
    that.data.showList=advancedata
    this.data.allEvents.forEach((item)=>{
      var temp=[]
      item.tags.forEach(v => {
       temp.push(v.type)
      })
      if(item.title.indexOf(data)>=0||temp.indexOf(data)>=0||item.map.indexOf(data)>=0)
      //判断是否包含
      {
        that.data.showList.push(item)
      }
    })
    //从新赋值
    that.setData({
      showList:that.data.showList,
    })

  }
}
})