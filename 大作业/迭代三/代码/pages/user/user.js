// pages/user/user.js
import Dialog from '../../miniprogram_npm/tdesign-miniprogram/dialog/index';

const app=getApp()
Page({
  data: {
    visible:false,
    usr_detail:{},
    message_num:"new",//消息数
    rank:"default",
    rank_star:0,
    tabs: ['未开始','进行中', '已结束'],
    current:0,
    value: '0',
    articledata1 : [],
    articledata2 : [],
    articledata3 : []
  },
  onOverlayClick(){
    this.setData({
      visible:false
    })
  },
  setting_event(){
    this.setData({
      visible:true
    })
  },
  click_point(){
    Dialog.alert({
      title: '积分规则',
      content: '1.签到一次+5分\n2.志愿每一小时+2分，一小时以内不计\n3.积分可换取相应礼物',
      confirmBtn: '我知道了',
    }).then(() => {
    });

  },
  delete(){
    Dialog.confirm({
      title: '确认注销',
      content: '注：该操作不可撤回，请谨慎操作。',
      confirmBtn: '确定',
      cancelBtn: '取消',
    })
      .then(() => {
        wx.cloud.callFunction({
          name:"DeleteUser",
          data:{
            openid:app.globalData.openid
          },
          success: async(res)=>{
            app.globalData.openid=""
            wx.navigateTo({
              url: '/pages/index/index',
            })
          }
        })
        
      })
      .catch(() => {
      });
  },
    onTabsChange(e) {
      this.setData({ value: e.detail.value })
    },
    refresh(){
      var that=this
      //查询数据库里是否有openid
      wx.cloud.database().collection("user")
      .where({
       openid:app.globalData.openid
        }).watch({
          onChange: async function(res){
            var ranks=["default","primary","success","warning","danger","danger","danger"]
            var temp=Math.floor(res.docs[0].serve_time/3600)
            res.docs[0].serve_time=(res.docs[0].serve_time/3600).toFixed(2)    
            that.setData({
             usr_detail:res.docs[0],
             rank_star:Math.floor(temp/100)+1,
             rank:ranks[parseInt(temp/100)]
           })
          }, 
          onError:function(err){
            console.log(err)
          }
        })
      wx.cloud.database().collection("relatives")
      .where({
       joiner_id:app.globalData.openid
        }).get().then(async res=>{
          let data=res.data
          data.forEach(async item => {
            wx.cloud.database().collection('events').where({
              event_num:item.event_id,
             })
             .get().then(res=>{   
               var temp1=[]    
               var temp2=[]  
               var temp3=[]      
               var start_time=new Date(res.data[0].time_start)
               var end_time=new Date(res.data[0].time_end)
               var time=new Date()
               if(time-start_time<0){
                temp1.push(res.data[0])            
               }
               else if(time-start_time>=0&&time-end_time<=0){
                temp2.push(res.data[0])
               }
               else if(time-end_time>0)
               {
                temp3.push(res.data[0])
              }
               this.setData({
                articledata1:temp1,
                articledata2:temp2,
                articledata3:temp3
               })
             }).catch(err=>{
               console.log("失败",err)
              })        
         })
     })
     .catch(err=>{
       console.log("失败",err)
     })   
    },
  onLoad(options) { 
    this.refresh()  
  },
  onShow(){
    this.refresh()  
  },
  create_event(){
    if(this.data.usr_detail.id==""){
      wx.showToast({
        title: '请先完善信息',
        icon:"none",
        duration:2000,
        mask:true
      })
    }
    else{
       wx.navigateTo({
      url: '/pages/event/event',
    })
    } 
  },
  messages(){
    if(this.data.usr_detail.id==""){
      wx.showToast({
        title: '请先完善信息',
        icon:"none",
        duration:2000,
        mask:true
      })
    }
    else{
      wx.navigateTo({
      url: '/pages/message/message',
    })
    }   
  },
  finish(){
    wx.navigateTo({
      url: '/pages/more/more?usr_detail='+JSON.stringify(this.data.usr_detail), 
    })
  },
  tabSelect:function(e){
    var current = e.currentTarget.dataset.id
    this.setData({
      current:current
    })
  },
})