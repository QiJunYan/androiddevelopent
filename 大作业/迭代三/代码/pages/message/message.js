// pages/message/message.js
const app=getApp()
const _=wx.cloud.database().command
Page({
  /**
   * 页面的初始数据
   */
  data: {
    currentTab:0,
    messages:[],
    event_messages:[],
    templist:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    var that=this
    wx.cloud.database().collection("messages").where(_.or([
{openid:app.globalData.openid},{userid:app.globalData.openid}
    ])
    ).get().then(res=>{
      var temp=res.data
      temp.forEach(async (item) => {
        if(item.message.length>=1){
           item.message[item.message.length-1].createTime=item.message[item.message.length-1].createTime.substring(11,16)
        }  
        var id = item.openid == app.globalData.openid ? item.userid : item.openid
        await wx.cloud.database().collection("user").where({
          openid: id
        }).get().then(res => {
          item.url = res.data[0].avatarUrl
          item.name = res.data[0].nickName
        })
        this.setData({
          messages:temp
        })
      })    
    }).catch(res=>{
      console.log(res)
    })

    wx.cloud.database().collection("events").where({
      creater_id:app.globalData.openid
    }).watch({
      onChange: async function(snop){
        var temp=snop.docs
        var event_msg=[]
        temp.forEach(item=>{
          wx.cloud.database().collection("relatives").where({
            event_id:item.event_num
          }).watch({
            onChange:async function(res){
              var t= await that.runs(res.docs) 
              event_msg.push({
                event_num:item.event_num,
                event_name:item.title,
                list:t,
                end_time:item.time_end,
                start_time:item.time_start,
                require_num:item.require_num,
                volunteer_num:item.volunteer_num
              })
              that.setData({
                event_messages:event_msg
              })
            },
            onError:function(err){
              console.log(err)
            }
          })
        }   
        )
      },
      onError:function(err){
        console.log(err)
      }

    })
  },
  async runs(event){
    var t=[]
    await event.forEach(it=>{
      wx.cloud.database().collection("user").where({
        openid:it.joiner_id
      }).get().then(res=>{
        t.push(res.data[0].avatarUrl)         
      })
    })
     return t
  },
  clickTab(){
    this.setData({
      event_messages:this.data.event_messages,
      currentTab:this.data.currentTab==1?0:1
    })
  },
  volunteer_list(e){
    wx.navigateTo({
      url: '/pages/volunteerlist/volunteerlist?event_num='+e.currentTarget.dataset.src.event_num+'&end_time='+e.currentTarget.dataset.src.end_time+'&start_time='+e.currentTarget.dataset.src.start_time,
    })
  },
  goTocontactPage(e){
    var temp=this.data.messages[e.currentTarget.dataset.id]
    var ui=temp.openid==app.globalData.openid?temp.userid:temp.openid
    wx.navigateTo({
      url: '/pages/messagesroom/messagesroom?openid='+app.globalData.openid+'&userid='+ui,
    })  
  }
})