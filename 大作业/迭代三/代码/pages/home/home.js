// pages/home/home.js
const app=getApp()
Page({
  data: {
    imgUrls: {},
    swiperIdx: 0,
    show:false
  },
  onShow(){
    this.refresh()
  },
  onLoad() {
    this.refresh()
  },
  refresh(){
    wx.cloud.callFunction({
      name:"UserSign",
      data:{
        openid:app.globalData.openid,
        time:new Date().toJSON().substring(0,10)+' '+new Date().toTimeString().substring(0,8)
      },
     success:res=>{
      if(res.result==1){
        this.setData({
          show:true
        })
      }
     }
    })
    wx.cloud.database().collection("imgs").get().then(res=>{
      this.setData({
        imgUrls:res.data
      })
    })
    .catch(err=>{
      console.log("失败",err)
    })
  },
  // 轮播特效果二
  bindchange(e) {
    this.setData({
      swiperIdx: e.detail.current
    })
  },
  popup:function(e){
    this.setData({
     show:!this.data.show
    })
    }

})