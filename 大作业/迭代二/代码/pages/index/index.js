const app=getApp()
Page({
  data: {
    userInfo: {},
    hasUserInfo: false,
    canIUseGetUserProfile: false,
  },
  onLoad() { 
    if(app.globalData.openid=="") {
      wx.cloud.callFunction({
        name:"GetOpenId",
        success: async(res)=>{
          const openId=res.result.openid
          app.globalData.openid=openId
          //查询数据库里是否有openid
        wx.cloud.database().collection("user")
       .where({
         openid:openId
          })
          .get().then(res=>{
           var l=res.data.length
           if(l==0){
            if (wx.getUserProfile) {
           this.setData({
          canIUseGetUserProfile: true
        })
    }
    }
    else{
      wx.switchTab({
        url: '/pages/home/home'
      })
    }    
  })
  .catch(err=>{
    console.log("失败",err)
  })
        }
      })
    }
    else{
      wx.switchTab({
        url: '/pages/home/home'
      })
    }
    
  },
  getUserProfile(e) {
    wx.getUserProfile({
      desc: '用于完善资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
        wx.setStorage({
          key:"avatarUrl",
          data:this.data.userInfo.avatarUrl,
        })
        wx.setStorage({
          key:"nickName",
          data:this.data.userInfo.nickName,
        })
        //将openid存入数据库
        wx.cloud.callFunction({
          name:"AddUserBasic",
          data:{
            openid:app.globalData.openid,
            avatarUrl:this.data.userInfo.avatarUrl,
            nickName:this.data.userInfo.nickName
         },
         success:res=>{
          console.log("成功",res)
          wx.switchTab({
            url: '/pages/home/home'
          })
         }
        })
      }
    })
  },
})