// serve.js

Page({
  data: {
   articledata : [{}]
  },
  onLoad(){
    this.refresh()
  },
  onShow(){
    this.refresh()
  },
  search(){
    wx.navigateTo({
      url: '/pages/search/search',
    })
  },
  refresh(){
    wx.cloud.database().collection("events").get().then(res=>{
      this.setData({
        articledata:res.data
      })
    })
    .catch(err=>{
      console.log("失败",err)
    })
  }
  })
  