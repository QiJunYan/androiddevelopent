// pages/more/more.js
import { IdCard } from '../../util/IdCard.js'
const watch =require("../../util/watch.js")
import Toast from '../../miniprogram_npm/tdesign-miniprogram/toast/index';
import Dialog from '../../miniprogram_npm/tdesign-miniprogram/dialog/index';

Page({
  data: {
    doc:"",
    name:'',
    id:"",
    declaration:"",
    age:"",
    birth:"",
    sex:""
  },
  onLoad(options) {
    watch.setWatcher(this)
    var event=JSON.parse(options.usr_detail)
    this.setData({
      name:event.nickName,
      doc:event._id,
      id:event.id,
      declaration:event.declaration,
      age:event.age,
      birth:event.birth,
      sex:event.sex
    })
  },
  confirm(){
    var that=this
    Dialog.confirm({
      title: '确认更改基本信息',
      content: '注：请勿频繁更改信息',
      confirmBtn: '确定',
      cancelBtn: '取消',
    })
    .then(() => {
      if(this.data.id==""||this.data.name==""){
        Toast({
          context: this,
          selector: '#t-toast',
          message: '请先完成必填信息',
          theme: 'fail',
        });
      }
      else{
        wx.showLoading({
          title: '更改中',
        }) 
        wx.cloud.callFunction({
          name:"AddUserDetail",
          data:{
            doc:that.data.doc,
            name:that.data.name,
            id:that.data.id,
            birth:that.data.birth,
            age:that.data.age,
            sex:that.data.sex,
            declaration:that.data.declaration
         },
         success:res=>{
          wx.hideLoading({})
          Toast({
            context: this,
            selector: '#t-toast',
            message: '提交成功等待审核',
            theme: 'success',
          });  
          wx.switchTab({
            url: '/pages/user/user'
          })       
         }
        })

      }
    })
    .catch(() => {
      // 点击取消按钮
    })
  },
  watch:{
    id:function(n,o){
      var age = IdCard(n, 3)
      var sex = IdCard(n, 2)
      var birth = IdCard(n, 1)
      if(n.length>=18){
        this.setData({
          age:age,
          birth:birth,
          sex:sex
        })
      }
    }
  },
})