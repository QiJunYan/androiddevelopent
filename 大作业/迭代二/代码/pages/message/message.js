// pages/message/message.js
const app=getApp()
const _=wx.cloud.database().command
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentTab:0,
    messages:[]

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.cloud.database().collection("messages").where(_.or([
{openid:app.globalData.openid},{userid:app.globalData.openid}
    ])
    ).get().then(res=>{
      var temp=res.data
      temp.forEach(async (item) => {
        if(item.message.length>=1){
           item.message[item.message.length-1].createTime=item.message[item.message.length-1].createTime.substring(11,16)
        }  
        var id = item.openid == app.globalData.openid ? item.userid : item.openid
        await wx.cloud.database().collection("user").where({
          openid: id
        }).get().then(res => {
          item.url = res.data[0].avatarUrl
          item.name = res.data[0].nickName
        })
        this.setData({
          messages:temp
        })
      })    
    }).catch(res=>{
      console.log(res)
    })
  },
  clickTab(){
    this.setData({
      currentTab:this.data.currentTab==1?0:1
    })
  },
  goTocontactPage(e){
    var temp=this.data.messages[e.currentTarget.dataset.id]
    var ui=temp.openid==app.globalData.openid?temp.userid:temp.openid
    wx.navigateTo({
      url: '/pages/messagesroom/messagesroom?openid='+app.globalData.openid+'&userid='+ui,
    })  
  }
})