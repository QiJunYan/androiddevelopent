// pages/home/home.js
Page({
  data: {
    imgUrls: {},
    swiperIdx: 0
  },
  onLoad() {
    wx.cloud.database().collection("imgs").get().then(res=>{
      console.log(res)
      this.setData({
        imgUrls:res.data
      })
    })
    .catch(err=>{
      console.log("失败",err)
    })
  },
  // 轮播特效果二
  bindchange(e) {
    this.setData({
      swiperIdx: e.detail.current
    })
  },

})