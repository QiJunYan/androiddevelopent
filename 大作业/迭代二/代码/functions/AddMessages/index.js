// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()

const db=cloud.database()
const _=db.command
// 云函数入口函数
exports.main = async (event, context) => {
  var temp=[]
  temp.push(event.openid)
  temp.push(event.userid)
  let num= await db.collection('messages').where({
    openid:_.in(temp),
    userid:_.in(temp)
  }).count()
  if(num.total==0){
     return await db.collection('messages').add({
   data:{
    openid:event.openid,
    userid:event.userid,
    message:[]
   }
 })
  }
  else{
    return -1
  }
}