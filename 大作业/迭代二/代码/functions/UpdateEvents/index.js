// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db=cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
 return await db.collection('events').doc(event.doc).update({
   data:{
    cover_img:event.cover_img,
    name:event.name,
    map:event.map,
    introduction:event.introduction,
    telephone:event.telephone,
    tags:event.tags,
    time_end:event.time_end,
    time_start:event.time_start,
    time_finish:event.time_finish,
    title:event.title,
    volunteer_num:event.volunteer_num,
    require_num:event.volunteer_num-event.temp_volunteer,
    gap_time:event.gap_time,
    latitude:event.latitude,
    longitude:event.longitude,
    address:event.address
   }
 })
}