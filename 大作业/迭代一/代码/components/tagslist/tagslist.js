Component({
  properties: {
    CurrentSelected: {
      type:Number,
      value:0,
      observer:function(newvalue){
        this.setData({
          selected:newvalue
        })
      }
    },
    AlreadySelected:{
      type: Array,
      value:{
       type: []
      },
      observer:'update',
    },
    tags: {
      type: Object,
      value:{
       type: ['']
      },
      multichoice: true //不能多选
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    selected:0,
    AlreadySelected:[],
    TagList:[]
  },
  lifetimes:{
    attached(){
      var that =this
      const NewTypeArr =[]
      if(this.properties.tags.multichoice){
        this.properties.tags.type.forEach(item => {   
          NewTypeArr.push({type:item,checked:false})
        })
        this.setData({
          TagList:NewTypeArr
        })       
      }
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    update(newvalue){
      this.setData({
        AlreadySelected:newvalue
      })
      if(this.properties.tags.multichoice){
        newvalue.forEach(item => {
          let index = this.properties.tags.type.findIndex(v => v==item.type)
          if(index!=-1){
            this.data.TagList[index].checked=true
          }
        })
        this.setData({
          TagList:this.data.TagList
        })      
      }
    },
    onClickSelectType(e){
      const {index} = e.currentTarget.dataset
      //console.log(index);
      //判断是否为多选
      if(this.properties.tags.multichoice==true){
        let SelectArr = this.data.AlreadySelected
        let taglist = this.data.TagList
        //判断是否选中
        let isselected = SelectArr.findIndex(item => this.properties.tags.type[index]==item.type)
      //  console.log(isselected);
        //已经选中，重复点击更改为未选中的状态
        if(isselected!=-1) {         
          SelectArr.splice(isselected,1);
          taglist[index].checked=false
          this.setData({
            selected:index,
            AlreadySelected:[].concat(SelectArr),
            TagList: [].concat(taglist)
          })
        }
        else{
          SelectArr.push({ type:this.properties.tags.type[index],id:index })
          taglist[index].checked=true
          this.setData({
            selected:index,
            AlreadySelected:[].concat(SelectArr),
            TagList: [].concat(taglist)
          })
        }
        //console.log(this.data.AlreadySelected);
        //选中的标签
        this.triggerEvent("onClickSelectTypeMulit",{type:this.data.AlreadySelected})
      }
      else{
        this.setData({
          selected:index
        })
        this.triggerEvent("onClickSelectType",{index,type:this.properties.tags.type[index]})
      }
     
     
    },
  }
})

