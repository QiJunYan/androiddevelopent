import Toast from '../../miniprogram_npm/tdesign-miniprogram/toast/index';
import Dialog from '../../miniprogram_npm/tdesign-miniprogram/dialog/index';

Page({
  data: {
    doc:"",
    temp_volunteer:0,
    selectedVolunteer:0,
    nums: [],
    cover_img:"",
    originFiles:[],
    datetimeVisible_start: false,
    datetimeVisible_end: false,
    datetimeVisible_finish:false,
    volunteer_num:false,
    datetime: '2022-05-10 12:30',
    latitude: 0, //首次加载维度
    longitude: 0, //首次加载的经度
    address:"",
    title:"",
    datetimeStart: '年 月 日 时',
    datetimeEnd: '年 月 日 时',
    mapName: "", //选点的位置
    datetimeFinish: '年 月 日 时',
    name:"",
    telephone:"",
    introduction:"",
    phoneError:false,

    // 指定选择区间起始值
    disableDateStart: {
      before: '2010-01-01 14:23',
      after: '2023-12-31 24',
    },
    disableDateEnd:{
      before: '2022-01-01 00',
      after: '2023-12-31 24',
    },
    disableDateFinish:{
      before: '2010-01-01 00',
      after: '2023-12-31 24',
    },
    MyTagsList: { 
      type: [ '防控疫情',"交通治安" ,"普法宣传","大型活动","环境保护","帮困助弱","文明实践","文体科技"],
      multichoice: true //多选，若单选不传参或为False
     },
    alreadyselect: false,
    SelectMyTag:[],
    state:0//0:确认提交 1:确认修改
  },
  changes(e){
    var reg=/\d/
    if(reg.test(e.detail.value)){
      this.setData({
        phoneError:true
      })
    }
    else{
      this.setData({
        phoneError:false
      })
    }
  },
  onLoad(options) {
    if(Object.keys(options).length!==0){
      var event=JSON.parse(options.event_detail)
      console.log(event)
      this.setData ({
              originFiles:event.cover_img,
              state:1,
              doc:event._id,
              temp_volunteer:event.volunteer_num-event.require_num,
              name:event.name,
              mapName:event.map,
              introduction:event.introduction,
              alreadyselect:true,
              SelectMyTag:event.tags,
              datetimeEnd:event.time_end,
              datetimeStart:event.time_start,
              datetimeFinish:event.time_finish,
              title:event.title,
              telephone:event.telephone,
              selectedVolunteer:event.volunteer_num,
              latitude:event.latitude,
              longitude:event.longitude,
              address:event.address,
              'disableDateEnd.before':event.time_start,
              'disableDateFinish.after':event.time_start,
      })
    }
    const temp=[]
    for (var i=1;i<=100;i++){
      temp.push({ label: i, value: i })
    }
    var today=new Date().toJSON().substring(0,10)+' '+new Date().toTimeString().substring(0,8)
    this.setData({
      nums:temp,
      'disableDateStart.before': today,
      'disableDateEnd.before': today,
      'disableDateFinish.before': today,
    });
  },
     //选择个人标签
     SelectMyTags(e){
      this.setData({
        SelectMyTag : e.detail.type,
        alreadyselect:true
      })
    },
  click_num(){
    this.setData({
      volunteer_num:true
  })
  },
  onPickerConfirm(e) {
    var that=this
    if( that.data.state==1&&that.data.temp_volunteer>e.detail.value[0]?.value){
      Toast({
        context: this,
        selector: '#t-toast',
        message: '已有'+that.data.temp_volunteer+'名志愿者报名',
        theme: 'fail',
      });
    }
    else{
        this.setData({
      volunteer_num:false,
      selectedVolunteer: e.detail.value[0]?.value,
    });
    }
  },
    moveToLocation() {
        let that = this
        wx.chooseLocation({
            success: function (res) {
                console.log(res);
                //赋值给data中的mapName
                that.setData({
                    mapName: res.name,
                    address:res.address,
                    latitude:res.latitude,
                    longitude:res.longitude
                })
            },
            //错误信息
            fail: function (err) {
                console.log(err);
            }
        })
    },
  click_time_start(){
    this.setData({
      datetimeVisible_start:true
    });
  },
  click_time_end(){
    if(this.data.datetimeStart=='年 月 日 时'){
      Toast({
        context: this,
        selector: '#t-toast',
        message: '请先设置起始时间',
        theme: 'fail',
      });
    }
    else{
       this.setData({
      datetimeVisible_end:true
    });
    } 
  },
  click_time_finish(){
    if(this.data.datetimeStart=='年 月 日 时'){
      Toast({
        context: this,
        selector: '#t-toast',
        message: '请先设置起止时间',
        theme: 'fail',
      });
    }
    else{
    this.setData({
      datetimeVisible_finish:true
    });}
  },
  onCancel(e){
    this.setData({
      datetimeVisible_start:false,
      datetimeVisible_end:false,
      volunteer_num:false,
      datetimeVisible_finish:false
    });
  },
  onConfirm_start(e) {
    const { value, formatValue } = e?.detail;
    this.setData({
      datetimeVisible_start: false,
      datetime: value.valueOf(),
      datetimeStart: formatValue,
      'disableDateEnd.before':value.valueOf(),
      'disableDateFinish.after':value.valueOf(),
    });
  },
  onConfirm_end(e){
    const { value, formatValue } = e?.detail;
    this.setData({
      datetimeVisible_end: false,
      datetime: value.valueOf(),
      datetimeEnd: formatValue,
    });
  },
  onConfirm_finish(e){
    const { value, formatValue } = e?.detail;
    this.setData({
      datetimeVisible_finish: false,
      datetime: value.valueOf(),
      datetimeFinish: formatValue,
    });
  },
  gap(usedTime){
    var that=this
    usedTime=new Date(that.data.datetimeEnd.replace(/-/g,'/'))-new Date(that.data.datetimeStart.replace(/-/g,'/'))
    var days = Math.floor(usedTime / (24 * 3600 * 1000));
    days=days==0?"":days+'天'
    //计算出小时数
    var leave1 = usedTime % (24 * 3600 * 1000); //计算天数后剩余的毫秒数
    var hours = Math.floor(leave1 / (3600 * 1000));//将剩余的毫秒数转化成小时数
    hours=hours==0?"":hours+'小时'
    // 计算相差分钟数
    var leave2 = leave1 % (3600 * 1000); //计算小时数后剩余的毫秒数
    var minutes = Math.floor(leave2 / (60 * 1000));//将剩余的毫秒数转化成分钟
    minutes=minutes==0?"":minutes+'分钟'
    var time = days + hours + minutes;
    time=time==""?"0分钟":time
    return time;
  },
  change(){
    var that=this
    var gap_time=this.gap()
    Dialog.confirm({
      title: '确认更改活动信息',
      content: '注：请勿频繁更改信息',
      confirmBtn: '确定',
      cancelBtn: '取消',
    })
    .then(() => {
      if(that.data.title==""||that.data.datetimeStart=='年 月 日 时'||that.data.datetimeEnd=='年 月 日 时'||that.data.mapName==""||that.data.datetimeFinish=='年 月 日 时'||that.data.name==""||that.data.telephone==""||that.data.SelectMyTag.length==0||that.data.introduction==""||that.data.selectedVolunteer==0||this.data.originFiles.length==0){
        Toast({
          context: this,
          selector: '#t-toast',
          message: '请先完成必填信息',
          theme: 'fail',
        });
      }
      else{
        wx.showLoading({
          title: '更改中',
        })
        console.log(that.data.originFiles[0].url.indexOf("cloud"))
        if(that.data.originFiles[0].url.indexOf("cloud")!=-1){
          console.log(that.data.originFiles)
          wx.cloud.callFunction({
            name:"UpdateEvents",
            data:{
              doc:that.data.doc,  
              cover_img:that.data.originFiles,
              temp_volunteer:that.data.temp_volunteer,
              name:that.data.name,
              map:that.data.mapName,
              introduction:that.data.introduction,
              tags:that.data.SelectMyTag,
              time_end:that.data.datetimeEnd,
              time_start:that.data.datetimeStart,
              time_finish:that.data.datetimeFinish,
              title:that.data.title,
              telephone:that.data.telephone,
              volunteer_num:that.data.selectedVolunteer,
              gap_time:gap_time,
              latitude:that.data.latitude,
              longitude:that.data.longitude,
              address:that.data.address
           },
           success:res=>{ 
            wx.hideLoading({})
            Toast({
              context: this,
              selector: '#t-toast',
              message: '更改成功等待审核',
              theme: 'success',
            });  
           wx.navigateBack({
             delta: 1,
             success:function(){
               console.log("成功")
             }
           })     
           }
          })
        }
        else{
          wx.cloud.uploadFile({
          cloudPath:"photo/"+Date.now()+".jpg",
          filePath:that.data.originFiles[0].url,
          success(res) {
            that.data.originFiles[0].url=res.fileID
             wx.cloud.callFunction({
          name:"UpdateEvents",
          data:{
            doc:that.data.doc,  
            cover_img:that.data.originFiles,
            temp_volunteer:that.data.temp_volunteer,
            name:that.data.name,
            map:that.data.mapName,
            introduction:that.data.introduction,
            tags:that.data.SelectMyTag,
            time_end:that.data.datetimeEnd,
            time_start:that.data.datetimeStart,
            time_finish:that.data.datetimeFinish,
            title:that.data.title,
            telephone:that.data.telephone,
            volunteer_num:that.data.selectedVolunteer,
            gap_time:gap_time,
            latitude:that.data.latitude,
            longitude:that.data.longitude,
            address:that.data.address
         },
         success:res=>{ 
          wx.hideLoading({})
          Toast({
            context: this,
            selector: '#t-toast',
            message: '更改成功等待审核',
            theme: 'success',
          });  
         wx.navigateBack({
           delta: 1,
           success:function(){
             console.log("成功")
           }
         })     
         }
        })
          }
        })
        }     
      }
    })
    .catch(() => {
      // 点击取消按钮
    })
  },
  create(){ 
    var that=this
    var gap_time=this.gap()
    console.log(gap_time)
    Dialog.confirm({
      title: '确认创建活动信息无误',
      content: '注：创建者可在活动详情页修改活动信息',
      confirmBtn: '确定',
      cancelBtn: '取消',
    })
      .then(() => {
        gap_time=this.gap()
        if(that.data.title==""||that.data.datetimeStart=='年 月 日 时'||that.data.datetimeEnd=='年 月 日 时'||that.data.mapName==""||that.data.datetimeFinish=='年 月 日 时'||that.data.name==""||that.data.telephone==""||that.data.SelectMyTag.length==0||that.data.introduction==""||that.data.selectedVolunteer==0||this.data.originFiles.length==0){
          Toast({
            context: this,
            selector: '#t-toast',
            message: '请先完成必填信息',
            theme: 'fail',
            actions:[],
            placement:""
          });
        }
        else{
          wx.showLoading({
            title: '提交中',
          })
          wx.cloud.uploadFile({
            cloudPath:"photo/"+Date.now()+".jpg",
            filePath:that.data.originFiles[0].url,
            success(res) {
              that.data.originFiles[0].url=res.fileID
              wx.cloud.callFunction({
                name:"AddEvents",
                data:{
                  cover_img:that.data.originFiles,
                  name:that.data.name,
                  map:that.data.mapName,
                  introduction:that.data.introduction,
                  tags:that.data.SelectMyTag,
                  time_end:that.data.datetimeEnd,
                  time_start:that.data.datetimeStart,
                  time_finish:that.data.datetimeFinish,
                  title:that.data.title,
                  telephone:that.data.telephone,
                  volunteer_num:that.data.selectedVolunteer,
                  gap_time:gap_time,
                  latitude:that.data.latitude,
                  longitude:that.data.longitude,
                  address:that.data.address
               },
               success:res=>{
                 console.log(res)
                wx.hideLoading({})
                wx.showToast({
                  title:"提交成功，请等待审核",
                  icon:"success",
                  duration:2000
                })
                wx.switchTab({
                  url: '/pages/user/user'
                })       
               }
              })
            },
            fail(res) {
              console.log(res)
              wx.hideLoading({})
              wx.showToast({
                title:"上传失败，请检查网络！",
                icon:"none",
                duration:2000
              })
            }
          }) 
        
        }
      })
      .catch(() => {
        // 点击取消按钮
      })
  },
handleRemove(e){
this.setData({
  originFiles:[]
})
  },
handleSucces(e){
    this.data.originFiles.push(e.detail.files[0])
    this.setData({
      originFiles:this.data.originFiles
    })
  },
    uploadPhotoToDatabase: function() {
        wx.cloud.uploadFile({
          cloudPath:"photo/"+Date.now()+".jpg",
          filePath:item,
          success(res) {
            console.log(res)
            wx.showToast({
              title:"上传成功！",
              duration:2000
            })
          },
          fail(res) {
            console.log(res)
            wx.showToast({
              title:"上传失败，请检查网络！",
              icon:"none",
              duration:2000
            })
          }
        })  
      
    },

});