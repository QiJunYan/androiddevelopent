// pages/home/home.js
const app=getApp()
Page({
  data: {
    imgUrls: {},
    swiperIdx: 0,
    show:false,
    volunteer_num:0,
    serve_time:0,
    activite_time:0
  },
  onShow(){
    this.refresh()
  },
  onLoad() {
    this.refresh()
  },
  refresh(){
    var that=this
    wx.cloud.callFunction({
      name:"UserSign",
      data:{
        openid:app.globalData.openid,
        time:new Date().toJSON().substring(0,10)+' '+new Date().toTimeString().substring(0,8)
      },
     success:res=>{
      if(res.result==1){
        this.setData({
          show:true
        })
      }
     }
    })
    wx.cloud.database().collection("imgs").get().then(res=>{
      this.setData({
        imgUrls:res.data
      })
      wx.cloud.database().collection("user").watch({
        onChange:async function(res){
          var temp_num=0
          var serve_time=0
          res.docs.forEach(item=>{
            temp_num+=1,
            serve_time+=item.serve_time/3600
            that.setData({
              volunteer_num:temp_num,
              serve_time:serve_time.toFixed(2)
            })
          })
        },
        onError:function(err){
          console.log(err)
        }
      })  
      wx.cloud.database().collection("events").watch({
        onChange:async function(res){
          that.setData({
            activite_time:res.docs.length
          })  
        },
        onError:function(err){
          console.log(err)
        }
      })  
      

    })
    .catch(err=>{
      console.log("失败",err)
    })
  },
  // 轮播特效果二
  bindchange(e) {
    this.setData({
      swiperIdx: e.detail.current
    })
  },
  popup:function(e){
    this.setData({
     show:!this.data.show
    })
    }

})