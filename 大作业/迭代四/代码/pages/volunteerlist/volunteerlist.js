// pages/volunteerlist/volunteerlist.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:0,
    volunteers:[],
    show_finish:false,
    gap:0
  },
  onLoad(options) {
        var that=this
    if(new Date(options.end_time.replace(/-/g,'/'))-new Date()<=0){
      that.setData({
        gap:new Date(options.end_time.replace(/-/g,'/'))-new Date(options.start_time.replace(/-/g,'/')),
        show_finish:true,
        id:options.event_num
      })
    }
    wx.cloud.database().collection("relatives").where({
      event_id:parseInt(options.event_num)
    }).watch({
      onChange:async function(res){
        var t=[]
    await res.docs.forEach(it=>{
      wx.cloud.database().collection("user").where({
        openid:it.joiner_id
      }).get().then(re=>{
        t.push(
          {info:re.data[0],
            state:it.state
          }
          )    
        that.setData({
          volunteers:t
        }) 
      })
    }
    )  
      },
      onError:function(err){
        console.log(err)
      }
    })
  },
  finish_btn(e){ 
    var that=this
    wx.showLoading({
      title: '提交中...',
    })
    wx.cloud.callFunction({
      name:"UpdateRelatives",
      data:{
        event_id:that.data.id,
        joiner_id:e.currentTarget.dataset.id
     },
     success:res=>{
      wx.cloud.callFunction({
        name:"UpdateUser",
        data:{
          open_id:e.currentTarget.dataset.id,
          gap:that.data.gap/1000
       },success:res=>{
         wx.hideLoading({
         })
      wx.showToast({
        title:"设置成功",
        duration:2000
      })
       }
      })
     }
    })
  }
})