// pages/detail/detail.js
import Dialog from '../../miniprogram_npm/tdesign-miniprogram/dialog/index';
import Toast from '../../miniprogram_npm/tdesign-miniprogram/toast/index';

const app=getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    event_detail:{},
    leave_time:"",
    state:-1, //0:立即报名，1:修改信息;2:时间截止;3:已报名
    id:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    var id=options.id
    this.refresh(id)
  },
  refresh(id){
    var that=this
    var state=0
    wx.cloud.database().collection("events")
    .where({
      event_num:parseInt(id)
       }).watch({
        onChange: async function(resource){
          wx.cloud.database().collection("relatives")
         .where({
          event_id:resource.docs[0].event_num,
          joiner_id:app.globalData.openid    
            })
            .get().then(res=>{
              var l=res.data.length
              if(l!==0){
                state=3
              }
              var time=that.gap(new Date(resource.docs[0].time_finish.replace(/-/g,'/'))-new Date())
              if(time=="报名已截止"){
                state=2
              }
              if(resource.docs[0].creater_id==app.globalData.openid){
                state=1;
              }
              that.setData({
                id:id,
                event_detail:resource.docs[0],
                leave_time:time,
                state:state
              })
              })
            .catch(err=>{
              console.log("失败",err)
            })   
        }, 
        onError:function(err){
          console.log(err)
        }
       })
  },
  gap(usedTime){
    if(usedTime<=0){
      this.setData({
        state:2
      })
      return "报名已截止"
    }
    var days = Math.floor(usedTime / (24 * 3600 * 1000));
    //计算出小时数
    var leave1 = usedTime % (24 * 3600 * 1000); //计算天数后剩余的毫秒数
    var hours = Math.floor(leave1 / (3600 * 1000));//将剩余的毫秒数转化成小时数
    days=hours>12?(days+1):days
    days=days==0?"0":days+""
    return days;
  },
  guide(){
    var that=this
    wx.getLocation({
      type:'wgs84',
      success:function(res){
            wx.openLocation({
      latitude: that.data.event_detail.latitude,
      longitude: that.data.event_detail.longitude,
      name:that.data.event_detail.map,
      address:that.data.event_detail.address
    })
      }
    })

  },
  call_num(){
    wx.makePhoneCall({
      phoneNumber: this.data.event_detail.telephone,
    })
  },
  share(){

  },
  join(){
    var that=this
    Dialog.confirm({
      title: '报名',
      content: '确认报名该活动',
      confirmBtn: '确定',
      cancelBtn: '取消',
    })
      .then(() => {
          var release=that.data.event_detail.require_num-1
          if(release>=0){     
            wx.requestSubscribeMessage({
              tmplIds: ['o9urWpIJB730twgk6PzP4SFjrBM9Dlgo5ZStqFembrM','3UjtcCNG6J1dVClZqsr9K0GbZP4ckC1ACrf4Cf25C4s'],
              success (res) { 
                console.log(res)
                if(res.o9urWpIJB730twgk6PzP4SFjrBM9Dlgo5ZStqFembrM=='accept'){
                  wx.showLoading({
                    title: '报名中',
                  })
                     wx.cloud.callFunction({
                   name:"DecreaseEventsNum",
                   data:{
                     doc:that.data.event_detail._id,
                     require_num:release,
                   },
                  success:res=>{
                  wx.cloud.callFunction({
                    name:"AddRelatives",
                    data:{
                      event_id:that.data.event_detail.event_num,
                      joiner_id:app.globalData.openid
                    },
                   success:res=>{         
                    wx.hideLoading({})
                    Toast({
                      context: this,
                      selector: '#t-toast',
                      message: '报名成功等待审核',
                      theme: 'success',
                    });  
                    that.refresh(that.data.id) 
                   }
                  })          
                  }
                 })   
              }
              }
            })       
          }    
          else{
            Toast({
              context: this,
              selector: '#t-toast',
              message: '招募人数已经满了',
              theme: 'fail',
            });  
          }
      })
      .catch(() => {
        // 点击取消按钮
      })
  },
  consult(){
    var that=this
    wx.cloud.callFunction({
      name:"AddMessages",
      data:{
        userid:that.data.event_detail.creater_id,
        openid:app.globalData.openid
      },
     success:res=>{
      wx.navigateTo({
        url: '/pages/messagesroom/messagesroom?openid='+app.globalData.openid+'&userid='+that.data.event_detail.creater_id,
      })           
     }
    })  
  },
  change(){
    wx.navigateTo({
      url: '/pages/event/event?event_detail='+JSON.stringify(this.data.event_detail), 
    })

  }
})