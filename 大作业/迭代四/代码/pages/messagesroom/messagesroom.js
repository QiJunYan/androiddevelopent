// pages/messagesroom/messagesroom.js
const _=wx.cloud.database().command
const app=getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    scrolltop:2000,
    visible:false,
    input_content:"",
    id:app.globalData.openid,
    Message:[],
    userurl:"",
    username:"",
    openurl:"",
    openname:"",
    doc:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    var that=this
      wx.cloud.callFunction({
        name:"GetOpenId",
        success: (res)=>{
          that.setData({    
            id: res.result.openid
          })
          var openid=options.openid==""?res.result.openid:options.openid
          wx.cloud.database().collection('messages').where(_.or([
            {openid:openid,userid:options.userid},{userid:openid,openid:options.userid}
                ])
                ).watch({
                  onChange: async function(snop){
                    var temp=snop.docs[0]
                    if(temp.openid!= that.data.id){
                    var t=temp.openid
                    temp.openid=temp.userid
                    temp.userid=t
                  }
                  await wx.cloud.database().collection("user").where({
                    openid: temp.openid
                  }).get().then(res => {
                    that.setData({
                      openurl : res.data[0].avatarUrl,
                      openname :res.data[0].nickName
                    })         
                  })
                  await wx.cloud.database().collection("user").where({
                    openid: temp.userid
                  }).get().then(res => {
                    wx.setNavigationBarTitle({
                      title: res.data[0].nickName,
                    })
                    that.setData({
                      userurl : res.data[0].avatarUrl,
                      username :res.data[0].nickName
                    })
                  })
                  that.setData({
                    Message:temp,
                    scrolltop:temp.message.length*200
                  })
                  },
                  onError:function(err){
                    console.log(err)
                  }
                }
                ) 
        }
        })
   
  },
  send(){
    var that=this
    if(this.data.input_content!="")
    {
      wx.cloud.callFunction({
        name:"UpdateMessages",
        data:{
          doc:that.data.Message._id,
          input_content:that.data.input_content,
          openid:that.data.id,
          time:new Date().toJSON().substring(0,10)+' '+new Date().toTimeString().substring(0,8),
          type:"text"
        },
        success: (res)=>{
          this.setData({
            input_content:""
           })
        }
      })
    }
   
  },
  bindChange(e){
    this.setData({
      input_content:e.detail.value
    })
  },
  upimg1(){
    this.setData({
      visible:true
    })
  },
  onOverlayClick(){
    this.setData({
      visible:false
    })
  },
  phoneshow(e){
    wx.previewImage({
      urls:[e.target.dataset.src]
    })
  },
  sendpic(){
    var that=this
      wx.chooseImage({
        count: 1,
        sizeType: ['original', 'compressed'],
        sourceType: ['album', 'camera'],
        success :(res) =>{ 
          wx.showLoading({
            title: '上传中',
          })  
          let item=res.tempFilePaths[0]
          let suffix = /\.\w+$/.exec(item)
        wx.cloud.uploadFile({
        cloudPath:`message/${Date.now()}-${Math.random()*1000}${suffix}`,
        filePath:item,
        success:(res)=>{ 
          wx.cloud.callFunction({
            name:"UpdateMessages",
            data:{
              doc:that.data.Message._id,
              input_content:res.fileID,
              openid:that.data.id,
              time:new Date().toJSON().substring(0,10)+' '+new Date().toTimeString().substring(0,8),
              type:"img"
            },
            success: (res)=>{
              wx.hideLoading({})  
              wx.showToast({
                title: '上传成功',
                duration:2000,
                mask:true
              })         
            }
          })
        },
        fail(res) {
          wx.showToast({
            title:"上传失败，请检查网络！",
            icon:"none",
            duration:2000
          })
        }
      })  
        }
      })  
  },
  sendvideo(){
    var that=this
    wx.chooseVideo({
      sourceType:['album','camera'], 
      camera:'back',
      compressed:true,
      success :(res) =>{  
        wx.showLoading({
          title: '上传中',
        })  
        let item=res.tempFilePath
        let suffix = /\.\w+$/.exec(item)
      wx.cloud.uploadFile({
      cloudPath:`video/${Date.now()}-${Math.random()*1000}${suffix}`,
      filePath:item,
      success:(res)=>{ 
        wx.cloud.callFunction({
          name:"UpdateMessages",
          data:{
            doc:that.data.Message._id,
            input_content:res.fileID,
            openid:that.data.id,
            time:new Date().toJSON().substring(0,10)+' '+new Date().toTimeString().substring(0,8),
            type:"video"
          },
          success: (res)=>{
            wx.hideLoading({
            })   
            wx.showToast({
              title: '上传成功',
              duration:2000,
              mask:true
            })      
          }
        })
      },
      fail(res) {
        wx.showToast({
          title:"上传失败，请检查网络！",
          icon:"none",
          duration:2000
        })
      }
    })  
      }
    })  
  },
  downloadfile(e){
    var that=this
    var temp=e.currentTarget.dataset.src.name
    var fileid=e.currentTarget.dataset.src.path
    wx.cloud.downloadFile({
      fileID:fileid, 
      success(res){
        wx.showLoading({
          title: '下载中',
        }) 
         that.setData({
           filePath: '/' +  temp + /\.[^\.]+$/.exec(res.tempFilePath)[0],
         })
        wx.getFileSystemManager().saveFile({
          tempFilePath: res.tempFilePath,
          filePath: wx.env.USER_DATA_PATH + that.data.filePath,
          success(res) {
            wx.hideLoading({
            })
            wx.showToast({
              title: '文件已保存至：' + res.savedFilePath,
              icon: 'none',
              duration: 1500
            })
            // 打开该文件
            wx.openDocument({
              filePath: res.savedFilePath,
              success: function (res) {
                console.log('打开文档成功')
              }
            })
          }
        })
      }
      })

  },
  sendcase(){
    var that=this
    wx.chooseMessageFile({
      count: 1,
      type:"file",
      success :(res) =>{  
        wx.showLoading({
          title: '上传中',
        })
        if(res.tempFiles[0].size>4194304){
          wx.hideLoading({})
          wx.showToast({
            title: '文件大小不能超过4MB',
            duration:2000,
            mask:true
          })
        }
        else{
          let item=res.tempFiles[0].path
          let filename=res.tempFiles[0].name
          let suffix = /\.\w+$/.exec(item)
        wx.cloud.uploadFile({
        cloudPath:`files/${Date.now()}-${Math.random()*1000}${suffix}`,
        filePath:item,
        success:(res)=>{ 
          let t={
            name:filename,
            path:res.fileID,
          }
          wx.cloud.callFunction({
            name:"UpdateMessages",
            data:{
              doc:that.data.Message._id,
              input_content:t,
              openid:that.data.id,
              time:new Date().toJSON().substring(0,10)+' '+new Date().toTimeString().substring(0,8),
              type:"file"
            },
            success: (res)=>{
              wx.hideLoading({})      
              wx.showToast({
                title: '上传成功',
                duration:2000,
                mask:true
              })    
            }
          })
        },
        fail(res) {
          wx.showToast({
            title:"上传失败，请检查网络！",
            icon:"none",
            duration:2000
          })
        }
      })  
        }
      }
    })  
  }

  
})