// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()

const db=cloud.database()
const _=db.command
// 云函数入口函数
exports.main = async (event, context) => {
  let num=await db.collection('user').where({
    openid:event.open_id,
  }).get()
  return db.collection("user").doc(num.data[0]._id)
  .update({
   data:{
     serve_time:num.data[0].serve_time+parseInt(event.gap),
     activity_num:num.data[0].activity_num+1,
     point:num.data[0].point+(Math.floor(parseInt(event.gap)/3600))*2
   }
 })
}