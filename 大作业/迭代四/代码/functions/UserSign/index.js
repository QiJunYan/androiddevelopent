// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()

const db=cloud.database()
const _=db.command
// 云函数入口函数
exports.main = async (event, context) => {
  let num=await db.collection('user').where({
    openid:event.openid
  }).get()
  if(num.data[0].lastlogin==""||(new Date(event.time.replace(/-/g,'/'))-new Date(num.data[0].lastlogin.replace(/-/g,'/')))/1000>86400){
    db.collection("user").doc(num.data[0]._id)
  .update({
   data:{
    lastlogin:event.time,
     point:num.data[0].point+5
   }
 })
    return 1
  }
  else{
    return 0
  }
}