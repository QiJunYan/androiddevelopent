// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db=cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  let num= await db.collection('events').count()
  num=num.total
  if(num==0){
    num=0
  }
  else{
    let data=await db.collection('events').get()
    num=data.data[num-1].event_num
  } 
  num=num+1
  await db.collection('events').add({
   data:{
    cover_img:event.cover_img,
    creater_id:cloud.getWXContext().OPENID,
    event_num:num,
    name:event.name,
    map:event.map,
    introduction:event.introduction,
    telephone:event.telephone,
    tags:event.tags,
    time_end:event.time_end,
    time_start:event.time_start,
    time_finish:event.time_finish,
    title:event.title,
    volunteer_num:event.volunteer_num,
    require_num:event.volunteer_num,
    gap_time:event.gap_time,
    latitude:event.latitude,
    longitude:event.longitude,
    address:event.address
   }
 })
 try {
  const result = await cloud.openapi.subscribeMessage.send({
      "touser": cloud.getWXContext().OPENID,
      "page": '/pages/detail/detail?id='+num,
      "lang": 'zh_CN',
      "data": {
        "phrase1": {
          "value": '通过'
        },
        "thing2": {
          "value": event.title
        },
        "time3": {
          "value": event.time_start
        },
        "thing16": {
          "value": event.map
        },
        "name4": {
          "value": event.name
        }
      },
      "templateId": 'oHHHAV0wRdpHlvU4UlrjjoTvlybND3kfRCEXIrzlga8',
      "miniprogramState": 'developer'
    })
  return result.errCode
} catch (err) {
  return err.errCode
}
}