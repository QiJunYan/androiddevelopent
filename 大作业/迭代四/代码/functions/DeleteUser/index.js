// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db=cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  let num= await db.collection('user').where({
    openid:event.openid
  }).get()
  num=num.data[0]._id
 return await db.collection('user').doc(num).remove()
}