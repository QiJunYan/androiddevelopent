// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()

const db=cloud.database()
const _=db.command
// 云函数入口函数
exports.main = async (event, context) => {
  let num=await db.collection('relatives').where({
    event_id:parseInt(event.event_id),
    joiner_id:event.joiner_id
  }).get()
   db.collection("relatives").doc(num.data[0]._id)
  .update({
   data:{
     state:1
   }
 })
 let data=await db.collection('events').where({
  event_num:parseInt(event.event_id),
}).get()
data=data.data[0]
 try {
  const result = await cloud.openapi.subscribeMessage.send({
      "touser": event.joiner_id,
      "page": '/pages/detail/detail?id='+event.event_id,
      "lang": 'zh_CN',
      "data": {
        "thing1": {
          "value": data.title
        }, 
        "thing2": {
          "value": data.map
        },  
        "time4": {
          "value":data.time_start+"~"+data.time_end,
        }, 
        "thing3": {
          "value": "志愿活动已结束，祝您生活愉快!"
        }
      },
      "templateId": '3UjtcCNG6J1dVClZqsr9K0GbZP4ckC1ACrf4Cf25C4s',
      "miniprogramState": 'developer'
    })
  return result.errCode
} catch (err) {
  return err.errCode
}
}