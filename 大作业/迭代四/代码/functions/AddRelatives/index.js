// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db=cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
 await db.collection('relatives').add({
   data:{
    event_id:event.event_id,
    joiner_id:event.joiner_id,
    state:0
   }
 })
 let data=await db.collection('events').where({
   event_num:event.event_id
 }).get()
 data=data.data[0]
 try {
  const result = await cloud.openapi.subscribeMessage.send({
      "touser": cloud.getWXContext().OPENID,
      "page": '/pages/detail/detail?id='+event.event_id,
      "lang": 'zh_CN',
      "data": {
        "thing2": {
          "value": data.title
        },
        "date4": {
          "value": data.time_start
        },
        "thing5": {
          "value": data.map
        },
        "thing9": {
          "value": data.name
        },
        "phone_number7": {
          "value": data.telephone
        }
      },
      "templateId": 'o9urWpIJB730twgk6PzP4SFjrBM9Dlgo5ZStqFembrM',
      "miniprogramState": 'developer'
    })
  return result.errCode
} catch (err) {
  return err.errCode
}
}