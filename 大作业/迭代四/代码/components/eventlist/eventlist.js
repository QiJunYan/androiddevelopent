Component({
  properties: {
    datas: {
      type:Array,
      value:{
       type: [{}]
      },
      observer:function(newvalue){
        this.setData({
          articles:newvalue
        })
      }
    }
  },
  /**
   * 组件的初始数据
   */
  data: { 
    articles:[],
  },
  methods: {
    event_detail(e){
      wx.navigateTo({
        url: '/pages/detail/detail?id='+e.currentTarget.dataset.id,
      })
    }
  }

})

